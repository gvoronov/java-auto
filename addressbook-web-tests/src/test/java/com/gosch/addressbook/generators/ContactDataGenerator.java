package com.gosch.addressbook.generators;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gosch.addressbook.model.ContactData;
import com.gosch.addressbook.model.GroupData;
import com.thoughtworks.xstream.XStream;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class ContactDataGenerator {

    @Parameter(names = {"-c", "--count"}, description = "Group Count")
    int count = 3;

    @Parameter(names = {"-p", "--path"}, description = "Destination Path")
    String path;

    @Parameter(names = {"-n", "--name"}, description = "File Name")
    String name = "contacts";

    @Parameter(names = {"-t", "--type"}, description = "File Type")
    String type = "csv";

    public static void main(String[] args) throws IOException {
        ContactDataGenerator generator = new ContactDataGenerator();
        JCommander jCommander = JCommander.newBuilder()
                .addObject(generator)
                .build();
        try {
            jCommander.parse(args);
        } catch (ParameterException ex) {
            jCommander.usage();
            return;
        }

        generator.run();
    }

    private void run() throws IOException {
        List<ContactData> contacts = generateContacts(count);

        if (type.equals("csv")) {
            saveAsCsv(contacts, new File(path + name + "." + type));
        } else if (type.equals("xml")) {
            saveAsXml(contacts, new File(path + name + "." + type));
        } else if (type.equals("json")) {
            saveAsJson(contacts, new File(path + name + "." + type));
        } else  {
            System.out.println("Unrecognized format: " + type);
        }
    }

    private void saveAsJson(List<ContactData> contacts, File file) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(contacts);
        try (Writer writer = new FileWriter(file)) {
            writer.write(json);
        }
    }

    private void saveAsXml(List<ContactData> contacts, File file) throws IOException {
        XStream xStream = new XStream();
        xStream.processAnnotations(ContactData.class);
        String xml = xStream.toXML(contacts);
        try (Writer writer = new FileWriter(file)) {
            writer.write(xml);
        }
    }

    private void saveAsCsv(List<ContactData> contacts, File file) throws IOException {
        try (Writer writer = new FileWriter(file)) {
            for (ContactData contact : contacts) {
                writer.write(String.format("%s;%s;%s;%s;%s;%s\n",
                        contact.getFirstName(),
                        contact.getLastName(),
                        contact.getEmail1(),
                        contact.getAddress(),
                        contact.getGroups().iterator().next().getName(),
                        contact.getPhoto()));
            }
        }
    }

    private List<ContactData> generateContacts(int count) {
        List<ContactData> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            list.add(new ContactData()
                    .withFirstName(String.format("name%s", i))
                    .withLastName(String.format("surname%s", i))
                    .withEmail1(String.format("email%s@email.com", i))
                    .withAddress(String.format("Vilde %s", i))
                    .inGroup(new GroupData().withName("test1"))
                    .withPhoto("src/test/resources/contact2.png"));
        }
        return list;
    }

}
