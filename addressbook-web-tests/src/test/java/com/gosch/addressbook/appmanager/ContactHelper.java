package com.gosch.addressbook.appmanager;

import com.gosch.addressbook.model.ContactData;
import com.gosch.addressbook.model.Contacts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class ContactHelper extends BaseHelper {

    public ContactHelper(WebDriver wd) {
        super(wd);
    }

    public void fillContactForm(ContactData contactData, boolean creation) {
        type(By.name("firstname"), contactData.getFirstName());
        type(By.name("lastname"), contactData.getLastName());
        type(By.name("home"), contactData.getHomePhone());
        type(By.name("mobile"), contactData.getMobilePhone());
        type(By.name("work"), contactData.getWorkPhone());
        type(By.name("email"), contactData.getEmail1());
        type(By.name("email2"), contactData.getEmail2());
        type(By.name("email3"), contactData.getEmail3());
        type(By.name("address"), contactData.getAddress());
        attach(By.name("photo"), contactData.getPhoto());

        if (creation) {
            if (contactData.getGroups().size() > 0) {
                Assert.assertTrue(contactData.getGroups().size() == 1);
                // Select is used for DropDown
                new Select(wd.findElement(By.name("new_group"))).selectByVisibleText(contactData.getGroups().iterator().next().getName());
            }
        } else {
            // In case of Contact Modification (creation == false)
            // Make sure there is no dropDown
            Assert.assertFalse(isElementPresent(By.name("new_group")));
        }
    }

    public void returnToHomePage() {
//        click(By.linkText("home page"));
        click(By.linkText("home"));
        Select select = new Select(wd.findElement(By.name("group")));
        select.selectByVisibleText("[all]");
    }

    public void initContactCreation() {
        click(By.linkText("add new"));
    }

    public void submitContactCreation() {
        click(By.name("submit"));
    }

    public void selectContact(int index) {
        wd.findElements(By.cssSelector("input[name='selected[]']")).get(index).click();
    }

    public void selectContactById(int id) {
        wd.findElement(By.cssSelector("input[value='" + id + "']")).click();
    }

    public void acceptContactsDeletion() {
        wd.switchTo().alert().accept();
        /*
        wait.withTimeout(10, TimeUnit.SECONDS)
                .withMessage("Davaj ponovoj")
                .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.msgbox")));
        */
    }

    public void deleteSelectedContacts() {
        click(By.cssSelector("input[value='Delete']"));
    }

    public void submitContactModification() {
        click(By.name("update"));
    }

    public void initContactModification(int index) {
        click(By.xpath("//table[@id='maintable']/tbody/tr[" + (index + 1) + "]/td[8]/a/img"));
    }

    public void initContactModificationById(int id) {
        /*
            Example #1: Метод последовательных приближений
         */
        WebElement checkbox = wd.findElement(By.cssSelector(String.format("input[value='%s']", id)));
        WebElement row = checkbox.findElement(By.xpath("./../.."));
        // точка означает начало с текущего элемента
        // два прыжка вверх по DOM [.. означают родительскую директорию]
        List<WebElement> cells = row.findElements(By.tagName("td"));
        cells.get(7).findElement(By.tagName("a")).click();

        /*
            Other Examples
         */
        //click(By.xpath("//*[@id='maintable']/tbody/tr[.//input[@value='" + id + "']]/td[8]/a/img"));
        //wd.findElement(By.xpath(String.format("//input[@value='%s']/../../td[8]/a", id))).click();
        //wd.findElement(By.xpath(String.format("//tr[.//input[@value='%s']]/td[8]/a", id))).click();
        //wd.findElement(By.cssSelector(String.format("a[href='edit.php?id=%s']", id))).click();
    }

    public void viewContactDetailsById(int id) {
        click(By.xpath("//*[@id='maintable']/tbody/tr[.//input[@value='" + id + "']]/td[7]/a/img"));
    }

    public void create(ContactData contact) {
        initContactCreation();
        fillContactForm(contact, true);
        submitContactCreation();
        contactCache = null;
        returnToHomePage();
    }

    public void modify(ContactData contact) {
        initContactModificationById(contact.getId());
        fillContactForm(contact, false);
        submitContactModification();
        contactCache = null;
        returnToHomePage();
    }

    public void delete(int index) {
        selectContact(index);
        deleteSelectedContacts();
        acceptContactsDeletion();
        contactCache = null;
        /*
        wait.withTimeout(10, TimeUnit.SECONDS)
                .withMessage("Error returning to Home Page")
                .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#search-az > form > input[type=\"text\"]")));
        */
    }

    public void delete(ContactData contact) {
        selectContactById(contact.getId());
        deleteSelectedContacts();
        contactCache = null;
        acceptContactsDeletion();
        /*
        wait.withTimeout(10, TimeUnit.SECONDS)
                .withMessage("Error returning to Home Page")
                .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#search-az > form > input[type=\"text\"]")));
        */
    }

    public void addContactToGroup(ContactData contact, int id) {
        selectContactById(contact.getId());
        Select select = new Select(wd.findElement(By.name("to_group")));
        select.selectByValue(Integer.toString(id));
        click(By.name("add"));
        returnToHomePage();
    }

    public boolean isThereAContact() {
        return isElementPresent(By.name("selected[]"));
    }

    public int count() {
        return wd.findElements(By.cssSelector("tr[name='entry']")).size();
    }

    public List<ContactData> list() {
        List<ContactData> contacts = new ArrayList<>();
        List<WebElement> elements = wd.findElements(By.name("entry"));
        for (WebElement element : elements) {
            List<WebElement> list = element.findElements(By.tagName("td"));
            contacts.add(new ContactData()
                    .withId(Integer.parseInt(list.get(0).findElement(By.tagName("input")).getAttribute("value")))
                    .withFirstName(list.get(2).getText())
                    .withLastName(list.get(1).getText())
                    .withEmail1(list.get(4).getText()));
        }
        return contacts;
    }

    private Contacts contactCache = null;

    public Contacts all() {
        if (contactCache != null) {
            return new Contacts(contactCache);
        }
        contactCache = new Contacts();
        List<WebElement> rows = wd.findElements(By.name("entry"));
        for (WebElement row : rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            contactCache.add(new ContactData()
                    .withId(Integer.parseInt(cells.get(0).findElement(By.tagName("input")).getAttribute("value")))
                    .withLastName(cells.get(1).getText())
                    .withFirstName(cells.get(2).getText())
                    .withAddress(cells.get(3).getText())
                    .withAllEmails(cells.get(4).getText())
                    .withAllPhones(cells.get(5).getText()));
        }
        return new Contacts(contactCache);
    }

    public ContactData infoFromEditForm(ContactData contact) {
        initContactModificationById(contact.getId());
        String firstName = wd.findElement(By.name("firstname")).getAttribute("value");
        String lastName = wd.findElement(By.name("lastname")).getAttribute("value");
        String address = wd.findElement(By.name("address")).getAttribute("value");
        String home = wd.findElement(By.name("home")).getAttribute("value");
        String mobile = wd.findElement(By.name("mobile")).getAttribute("value");
        String work = wd.findElement(By.name("work")).getAttribute("value");
        String email1 = wd.findElement(By.name("email")).getAttribute("value");
        String email2 = wd.findElement(By.name("email2")).getAttribute("value");
        String email3 = wd.findElement(By.name("email3")).getAttribute("value");
        wd.navigate().back();
        return new ContactData().withId(contact.getId()).withFirstName(firstName).withLastName(lastName).withHomePhone(home)
                .withMobilePhone(mobile).withWorkPhone(work).withAddress(address).withEmail1(email1).withEmail2(email2).withEmail3(email3);
    }

    public List<String> getContactGroups(ContactData contact) {
        List<String> groups = new ArrayList<>();
        viewContactDetailsById(contact.getId());
        List<WebElement> elements = wd.findElements(By.cssSelector("#container #content i > a"));
        for (WebElement element : elements) {
            String href = element.getAttribute("href");
            int delimiter = href.indexOf("=");
            groups.add(href.substring(delimiter + 1));
        }
        returnToHomePage();
        return groups;
    }

    public List<String> getAllAvailableGroups() {
        List<String> groups = new ArrayList<>();
        Select select = new Select(wd.findElement(By.name("to_group")));
        List<WebElement> options = select.getOptions();
        for (WebElement option : options) {
            groups.add(option.getAttribute("value"));
        }
        returnToHomePage();
        return groups;
    }

    public void removeContactFromGroup(ContactData contact, String group) {
        Select select = new Select(wd.findElement(By.name("group")));
        select.selectByValue(group);
        selectContactById(contact.getId());
        click(By.name("remove"));
        returnToHomePage();
    }

}
