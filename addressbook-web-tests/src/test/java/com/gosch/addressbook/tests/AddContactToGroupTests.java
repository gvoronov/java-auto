package com.gosch.addressbook.tests;

import com.gosch.addressbook.model.ContactData;
import com.gosch.addressbook.model.Contacts;
import com.gosch.addressbook.model.GroupData;
import com.gosch.addressbook.model.Groups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class AddContactToGroupTests extends TestBase {

    @BeforeMethod
    public void ensurePreconditions() {
        if (app.db().groups().size() == 0) {
            app.goTo().groupPage();
            app.group().create(new GroupData().withName("test1"));
        }
        if (app.db().contacts().size() == 0) {
            app.goTo().homePage();
            app.contact().create(new ContactData()
                    .withFirstName("Georgi")
                    .withLastName("Voronov")
                    .withEmail1("gv@test.com")
                    .withAddress("Vilde 88"));
        }
    }

    @Test
    public void testAddContactToGroup() {
        Contacts contacts = app.db().contacts();

        ContactData contact = contacts.iterator().next();
        List<String> contactGroupsBefore = app.contact().getContactGroups(contact);

        app.goTo().homePage();
        List<String> allAvailableGroups = app.contact().getAllAvailableGroups();

        if (allAvailableGroups.size() == contactGroupsBefore.size()) {
            GroupData group = new GroupData().withName("group1");
            app.goTo().groupPage();
            app.group().create(group);
            Groups groups = app.db().groups();
            group.withId(groups.stream().mapToInt((g) -> g.getId()).max().getAsInt());
            app.goTo().homePage();
            app.contact().addContactToGroup(contact, group.getId());
        } else {
            allAvailableGroups.removeAll(contactGroupsBefore);
            app.contact().addContactToGroup(contact, Integer.parseInt(allAvailableGroups.iterator().next()));
        }

        List<String> contactGroupsAfter = app.contact().getContactGroups(contact);
        assertThat(contactGroupsAfter.size(), equalTo(contactGroupsBefore.size() + 1));
    }

}
