package com.gosch.addressbook.tests;

import com.gosch.addressbook.appmanager.ApplicationManager;
import com.gosch.addressbook.model.ContactData;
import com.gosch.addressbook.model.Contacts;
import com.gosch.addressbook.model.GroupData;
import com.gosch.addressbook.model.Groups;
import org.openqa.selenium.remote.BrowserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestBase {

    Logger logger = LoggerFactory.getLogger(TestBase.class);

    // static - чтобы ссылка была общей для всех тестов
    protected static final ApplicationManager app = new ApplicationManager(
            System.getProperty("browser", BrowserType.CHROME),
            System.getProperty("target", "local"));

    // BeforeMethod - запуск перед каждым тестовым методом помеченым @Test
    // BeforeClass - запускается один раз перед всеми тестовыми методами, которые входят в какой-то класс
    // BeforeGroups - в TestNG есть возможность указать что тестовый метод принадлежит к какой-то группе,
    // тем самым для каждой группы можно написать свой собственный метод инициализации
    // BeforeSuite - в TestNG он всегда один единственный, соответствует одному запуску и он может состоять из нескольких тестов
    // Сделать Suite состоящий из нескольких тестовых методов можно только благодаря конфигурационного файла
    // Если запускать тесты из среды разработки по правой кнопки на пакет или на класс, то Suite создаётся автоматически,
    // и он состоит из одного единственного теста, в этом случае между Suite и Test нету разницы
    // Разница возникает, только в том случае, когда используется конфигурационный файл.
    // Suite всегда один единственный, Test - может быть несколько; Test - часть Suite-а;
    // Test включает в себя какие-то Class-ы, Class состоит из нескольких тестовых методов;
    // Hierarchy: Suit->Test->Class->Method

    @BeforeSuite
    public void setUp() throws Exception {
        app.init();
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() {
        app.stop();
    }

    @BeforeMethod
    public void logTestStart(Method method, Object[] params) {
        logger.info("Start test: " + method.getName() + " with parameters " + Arrays.asList(params));
    }

    @AfterMethod(alwaysRun = true)
    public void logTestStop(Method method) {
        logger.info("Stop test: " + method.getName());
    }

    public void verifyGroupListInUI() {
        // System.getProperty("verifyUI")
        if (Boolean.getBoolean("verifyUI")) {
            Groups dbGroups = app.db().groups();
            Groups uiGroups = app.group().all();
            assertThat(uiGroups, equalTo(dbGroups.stream()
                    .map((g) -> new GroupData().withId(g.getId()).withName(g.getName()))
                    .collect(Collectors.toSet())));
        }
    }

    public void verifyContactListInUI() {
        // System.getProperty("verifyUI")
        if (Boolean.getBoolean("verifyUI")) {
            Contacts dbContacts = app.db().contacts();
            Contacts uiContacts = app.contact().all();
            assertThat(uiContacts, equalTo(dbContacts.stream()
                    .map((c) -> new ContactData()
                            .withId(c.getId())
                            .withFirstName(c.getFirstName())
                            .withLastName(c.getLastName())
                            .withAddress(c.getAddress())
                            .withEmail1(c.getEmail1())
                            .withEmail2(c.getEmail2())
                            .withEmail3(c.getEmail3())
                            .mergeEmails()
                            .withWorkPhone(c.getWorkPhone())
                            .withMobilePhone(c.getMobilePhone())
                            .withHomePhone(c.getHomePhone())
                            .mergePhones())
                    .collect(Collectors.toSet())));
        }
    }

}
