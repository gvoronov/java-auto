package com.gosch.addressbook.tests;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.gosch.addressbook.model.ContactData;
import com.gosch.addressbook.model.Contacts;
import com.gosch.addressbook.model.GroupData;
import com.gosch.addressbook.model.Groups;
import com.thoughtworks.xstream.XStream;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ContactCreationTests extends TestBase {

    private final String group = "test1";
    private String photo = "src/test/resources/contact2.png";

    @BeforeMethod
    public void ensurePreconditions() {
        if (app.db().groups().size() == 0) {
            app.goTo().groupPage();
            app.group().create(new GroupData().withName(group));
        }
    }

    @DataProvider(name = "contacts")
    public Iterator<Object[]> contactsData() {
        List<Object[]> list = new ArrayList<>();
        Groups groups = app.db().groups();
        list.add(new Object[] {new ContactData().withFirstName("contact1").withLastName("contact1").withEmail1("contact1@cont.com").withAddress("Vilde1").withPhoto(photo).inGroup(groups.iterator().next())});
        list.add(new Object[] {new ContactData().withFirstName("contact2").withLastName("contact2").withEmail1("contact2@cont.com").withAddress("Vilde2").withPhoto(photo).inGroup(groups.iterator().next())});
        list.add(new Object[] {new ContactData().withFirstName("contact3").withLastName("contact3").withEmail1("contact3@cont.com").withAddress("Vilde3").withPhoto(photo).inGroup(groups.iterator().next())});
        return list.iterator();
    }

    @DataProvider(name = "contactsCSV")
    public Iterator<Object[]> contactsDataFromCsv() throws IOException {
        List<Object[]> list = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(new File("src/test/resources/contacts.csv")))) {
            String line = reader.readLine();
            while (line != null) {
                String[] split = line.split(";");
                list.add(new Object[] {new ContactData()
                        .withFirstName(split[0])
                        .withLastName(split[1])
                        .withEmail1(split[2])
                        .withAddress(split[3])
                        .inGroup(new GroupData().withName(split[4]))
                        .withPhoto(split[5])});
                line = reader.readLine();
            }
            return list.iterator();
        }
    }

    @DataProvider(name = "contactsXML")
    public Iterator<Object[]> contactsDataFromXml() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(new File("src/test/resources/contacts.xml")))) {
            String xml = "";
            String line = reader.readLine();
            while (line != null) {
                xml += line;
                line = reader.readLine();
            }
            XStream xStream = new XStream();
            xStream.processAnnotations(ContactData.class);
            List<ContactData> contacts = (List<ContactData>) xStream.fromXML(xml);
            return contacts.stream().map((c) -> new Object[]{c}).collect(Collectors.toList()).iterator();
        }
    }

    @DataProvider(name = "contactsJSON")
    public Iterator<Object[]> contactsDataFromJson() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(new File("src/test/resources/contacts.json")))) {
            String json = "";
            String line = reader.readLine();
            while (line != null) {
                json += line;
                line = reader.readLine();
            }
            Gson gson = new Gson();
            List<ContactData> contacts = gson.fromJson(json, new TypeToken<List<ContactData>>(){}.getType());
            return contacts.stream().map((c) -> new Object[]{c}).collect(Collectors.toList()).iterator();
        }
    }

    @Test(dataProvider = "contactsJSON")
    public void testContactCreationWithData(ContactData contact) {
        Contacts before = app.db().contacts();
        app.goTo().homePage();
        app.contact().create(contact);

        assertThat(app.contact().count(), equalTo(before.size() + 1));
        Contacts after = app.db().contacts();
        assertThat(after, equalTo(
                before.withAdded(contact.withId(after.stream().mapToInt((c) -> c.getId()).max().getAsInt()))));
        verifyContactListInUI();
    }

    @Test
    public void testContactCreation() {
        Groups groups = app.db().groups();
        String photo = "src/test/resources/contact2.png";
        ContactData contact = new ContactData()
                .withFirstName("Georgi")
                .withLastName("Voronov")
                .withEmail1("gv@test.com")
                .withAddress("Vilde 88")
                .withPhoto(photo)
                .inGroup(groups.iterator().next());

        Contacts before = app.db().contacts();
        app.goTo().homePage();
        app.contact().create(contact);

        assertThat(app.contact().count(), equalTo(before.size() + 1));
        Contacts after = app.db().contacts();
        assertThat(after, equalTo(
                before.withAdded(contact.withId(after.stream().mapToInt((c) -> c.getId()).max().getAsInt()))));
        verifyContactListInUI();
    }

    @Test
    public void testBadContactCreation() {
        Groups groups = app.db().groups();
        ContactData contact = new ContactData()
                .withFirstName("Georgi'")
                .withLastName("Voronov")
                .withEmail1("gv@test.com")
                .withAddress("Vilde 88")
                .inGroup(groups.iterator().next());
        Contacts before = app.db().contacts();
        app.goTo().homePage();
        app.contact().create(contact);

        assertThat(app.contact().count(), equalTo(before.size()));
        Contacts after = app.db().contacts();
        assertThat(after, equalTo(before));
        verifyContactListInUI();
    }

}
