package com.gosch.addressbook.tests;

import com.gosch.addressbook.model.ContactData;
import com.gosch.addressbook.model.Contacts;
import com.gosch.addressbook.model.GroupData;
import com.gosch.addressbook.model.Groups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ContactModificationTests extends TestBase {

    @BeforeMethod
    public void ensurePreconditions() {
        if (app.db().groups().size() == 0) {
            app.goTo().groupPage();
            app.group().create(new GroupData().withName("test1"));
        }
        if (app.db().contacts().size() == 0) {
            Groups groups = app.db().groups();
            app.goTo().homePage();
            app.contact().create(new ContactData()
                    .withFirstName("Georgi")
                    .withLastName("Voronov")
                    .withEmail1("gv@test.com")
                    .withAddress("Vilde 88")
                    .inGroup(groups.iterator().next()));
        }
    }

    @Test
    public void testContactModification() {
        Contacts before = app.db().contacts();
        ContactData modifiedContact = before.iterator().next();
        ContactData contact = new ContactData()
                .withId(modifiedContact.getId())
                .withFirstName("Mihhail")
                .withLastName("Alijev")
                .withEmail1("m@a.com")
                .withAddress("Kopli 96B");
        app.contact().modify(contact);

        assertThat(app.contact().count(), equalTo(before.size()));
        Contacts after = app.db().contacts();
        assertThat(after, equalTo(before.without(modifiedContact).withAdded(contact)));
        verifyContactListInUI();
    }

}
