package com.gosch.addressbook.tests;

import com.gosch.addressbook.model.ContactData;
import com.gosch.addressbook.model.Contacts;
import com.gosch.addressbook.model.GroupData;
import com.gosch.addressbook.model.Groups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class RemoveGroupFromContactTests extends TestBase {

    @BeforeMethod
    public void ensurePreconditions() {
        if (app.db().groups().size() == 0) {
            app.goTo().groupPage();
            app.group().create(new GroupData().withName("test1"));
        }
        if (app.db().contacts().size() == 0) {
            Groups groups = app.db().groups();
            app.goTo().homePage();
            app.contact().create(new ContactData()
                    .withFirstName("Georgi")
                    .withLastName("Voronov")
                    .withEmail1("gv@test.com")
                    .withAddress("Vilde 88")
                    .inGroup(groups.iterator().next()));
        }
    }

    @Test
    public void testRemoveGroupFromContact() {
        Contacts contacts = app.db().contacts();
        ContactData contact = contacts.iterator().next();

        app.goTo().homePage();
        List<String> contactGroups = app.contact().getContactGroups(contact);

        if (contactGroups.size() == 0) {
            Groups groups = app.db().groups();
            app.contact().addContactToGroup(contact, groups.iterator().next().getId());
            contactGroups = app.contact().getContactGroups(contact);
        }
        app.contact().removeContactFromGroup(contact, contactGroups.iterator().next());
        List<String> contactGroupsAfter = app.contact().getContactGroups(contact);

        assertThat(contactGroupsAfter.size(), equalTo(contactGroups.size() - 1));
    }

}
