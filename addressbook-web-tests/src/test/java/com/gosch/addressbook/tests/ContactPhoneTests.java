package com.gosch.addressbook.tests;

import com.gosch.addressbook.model.ContactData;
import com.gosch.addressbook.model.GroupData;
import com.gosch.addressbook.model.Groups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ContactPhoneTests extends TestBase {

    @BeforeMethod
    public void ensurePreconditions() {
        if (app.db().groups().size() == 0) {
            app.goTo().groupPage();
            app.group().create(new GroupData().withName("test1"));
        }
        if (app.db().contacts().size() == 0) {
            Groups groups = app.db().groups();
            app.goTo().homePage();
            app.contact().create(new ContactData()
                    .withFirstName("Georgi")
                    .withLastName("Voronov")
                    .withHomePhone("+7 (111) 648 70 33")
                    .withMobilePhone("+372 555 88 99")
                    .withWorkPhone("567-89-112")
                    .withEmail1("gv@test.com")
                    .withAddress("Vilde 88")
                    .inGroup(groups.iterator().next()));
        }
    }

    @Test
    public void testContactPhones() {
        app.goTo().homePage();
        ContactData contact = app.contact().all().iterator().next();
        ContactData contactInfoFromEditForm = app.contact().infoFromEditForm(contact);

        assertThat(contact.getAllPhones(), equalTo(contactInfoFromEditForm.mergePhones().getAllPhones()));
    }

}
