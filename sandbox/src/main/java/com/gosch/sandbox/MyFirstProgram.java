package com.gosch.sandbox;

public class MyFirstProgram {

    public static void main(String[] args) {
        System.out.println("Hello, world!");
        System.out.println(distance(new Point(2, 3), new Point(2, 3)));

        double side = 5;
        System.out.println("Area of square with side " + side + " = " + area(side));
    }

    public static double area(double side) {
        return side * side;
    }

    public static double area(double sideA, double sideB) {
        return sideA * sideB;
    }

    public static double distance(Point p1, Point p2) {
        return Math.sqrt(Math.pow((p2.x - p1.x), 2) + Math.pow((p2.y - p1.y), 2));
    }

}