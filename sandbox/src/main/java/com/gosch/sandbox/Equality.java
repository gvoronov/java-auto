package com.gosch.sandbox;

public class Equality {

    public static void main(String[] args) {
        String s1 = "firefox";
        String s2 = new String(s1);
        String s3 = "firefox";
        String s4 = "fire" + "fox";

        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));

        // Компилятор опитимизирует код. Все переменные, которые ссылаются на одну и ту же литеральную строку будут
        // ссылаться на один и тот же физический объект.
        System.out.println(s1 == s3);
        System.out.println(s1 == s4);
    }

}
